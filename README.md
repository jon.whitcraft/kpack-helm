# Kpack Helm Chart

A unofficial helm chart for installing kpack with Helm. By default, this chart will only install the CRDs. Any additional components (`Builder`, `ClusterStack`, etc) will need to be added via `--set <component>.create=true`

## Example

```
helm install --upgrade kpack kpack/kpack \

  --set serviceAccount.create=true \
  --set serviceAccount.secrets={my-secret,my-other-secret} \
  --set serviceAccount.imagePullSecrets={my-secret,my-other-secret} \
  --set serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn="<AWS ROLE ARN>" \

  --set clusterStore.create=true \
  --set clusterStore.images={"gcr.io/paketo-buildpacks/nodejs"} \

  --set clusterStack.create=true \
  --set clusterStack.id="io.buildpacks.stacks.bionic" \
  --set clusterStack.buildImage="paketobuildpacks/build:base-cnb" \
  --set clusterStack.runImage="paketobuildpacks/build:run-cnb" \

  --set builder.create=true \
  --set builder.tag="registry/my-builder" \
  --set builder.order={"paketo-buildpacks/nodejs"} \

  --set clusterBuilder.create=true \
  --set clusterBuilder.tag="registry/my-cluster-builder" \
  --set clusterBuilder.order={"paketo-buildpacks/nodejs"} \

  --set image.create=true \
  --set image.builderKind="ClusterBuilder" \
  --set image.name="my-app" \
  --set image.tag="registry/my-app" \
  --set image.source.git.url="git@gitlab.com/andrew.duss/my-app"
```

## Getting Started

1. Add this helm repo, update.
2. Add any secrets to your cluster. See [kpack Secrets Docs](https://github.com/pivotal/kpack/blob/main/docs/secrets.md) for more information. These secrets must be in the same namespace as that defined in the `values.yaml` file.

## Specifiying Buildpacks

1. Change the `clusterStore.images` and `builder.order` (or `clusterBuilder.order` if using a ClusterBuilder) entries in your `values.yaml` file

## Creating a Helm Chart Release

1. Bump the version in `Chart.yaml`
2. Package the helm chart into a targz:

```
helm package --destination ./releases .
```

3. Publish the chart to Gitlab. Be sure to change `kpack-0.1.0.tgz` and `development` with the correct version and channel of the chart.

```
curl -X POST \
  --form 'chart=@releases/kpack-1.1.4.tgz' \
  --user jon.whitcraft:$GITLAB_TOKEN \
  https://gitlab.com/api/v4/projects/43479450/packages/helm/api/development/charts
``
